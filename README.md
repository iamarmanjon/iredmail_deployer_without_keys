# iRedmail Deployer
---
The goal of this repo is to automate the process of installing iRedmail with
the common setup/configuration we are doing when we are setting it up. 

There are two steps on this process, Installing and Configuration, below are the
steps I usually take when I used the scripts in this repo.

### Steps:
1. Determine what domain you want to use as an smtp.

2. Pick a VPS where you want to install, make sure it's Ubuntu 14.04 x64.
Rebuild/Reinstall if necessary.

2. Got to DNSMadeEasy and assign the VPS IP to the domain (Create an A record)
3. Ping the domain you pick in Step #1.

4. Login to `monitoring.univposts.com` server

5. Goto the `iredmail_deployer` folder

6. SSH to your domain you pick in Step #1 to make sure you can access it with the
correct password.

7. When you're login run `apt-get update` command to update repositories

8. Exit the SSH session and go back to the `iredmail_deployer` folder in
`monitoring.univposts.com`.

9. Run the `install.sh` script with `./install.sh <smtp domain>`
e.g. `./install.sh example.com`

10. Enter the password when required.

11. After the installation, your server/smtp will restart. Wait for it to be online
and SSH again to the server.
NOTE: You should not be required to enter a password anymore, if it requires
you, there's someting wrong with your installation.

12. When logged in, run the following command to check if these services are running:
  - `service mysql status`
  - `service postfix status`
  - `service amavis status`
If all are running, then you're good to go on the configuration part of the setup.

13. Exit your server and go back to `iredmail_deployer` folder

14. Determine what domain you want to use for your sender e.g. mary@univmails.com
then the sender domain is univmails.com

15. Goto DNSMadeEasy and add the following text to the TXT records of your sender
domain. Replace "example.com", with your sender domain.

Name
>
  example.com._domainkey
  
Value
>
  "v=DKIM1; p=""MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsy4dmfRXeReo1xMyKt6r""uJoOuTnZjeOkQs/iCc6d06RkAi59feEQScyHHFfgZIsBHIq9hmKQKjgDiMD7Djzk""61BBnumd0YOsfwMMDu9v6ml3jn1z5Kz1wv749KTaIaGKlR/V+xlr19ICHZBGj6sr""MRkaxNjbgF+7rlHUF4Xxltq4/Wd4lbs+gB+9Yp2MD5tgKC3RRUjV09jGk2AAi0Xy""XNI3Ag7OjJjLO8nMcpA5r19g/9vdXM5CTpz0VYM+tVkSUPAn+/Dh+kLah54o49gT""Sh4OC41PbWtBww3l9/UBoOPFl2xVKVGCMJ1do+rtAiXYfHwBC2cD7a9Jk0VjyLON""mQIDAQAB"

Continuation:

16. Add also the IP of your smtp server to the the SPF record for the sender domain, just add ip4:123.123.123.123, where the 123.123.123 is your IP address.

17. Run `./config.sh <smtp domain> <sender domain>` e.g. `./config.sh example.com univmails.com`

18. SSH again in your server and run `amavisd-new testkeys`, you should see one
with "pass"

19. Run `tail -f /var/log/mail.log` to monitor your log file while you test your
smtp with sendy.

The usual credentials when an SMTP is setup are as follows:
  - host: yoursmtpdomain.com
  - username: mary@yoursmtpdomain.com
  - password: blastaway2016
  - port: 587
  - tls

20. Send yourself a sample email from sendy and check the email if SPF and DKIM
are passing.

THAT'S IT, YOU HAVE NOW A FUNCTIONING SMTP READY!